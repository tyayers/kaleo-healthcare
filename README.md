# Kaleo Healthcare Demo

**LIVE Demo here:** [https://tyayers.gitlab.io/kaleo-healthcare/](https://tyayers.gitlab.io/kaleo-healthcare/)

This project shows how a sample backend service delivering a lot of data can be optimized using API proxies for web clients.  The target of the optimization is to reduce the amount of data sent to clients, and use caching in the proxy to prevent expensive backend calls to be made when the data is already present in the cache.

## Background

This project is based on an [AppSheet Hospital Resources app](https://www.appsheet.com/samples/Resource-selfreporting-for-county-or-statewide-systems?appGuidString=78a5e4d4-2740-44bb-b5ab-476c4e2dc2d4) that was made in a Covid hackathon, regarding how hospital supplies can be managed across locations.  The app is really awesome, managing the resources in simple Google Sheets, and making the administration and entry of data as easy and direct as possible.

![Covid AppSheet App](img/appsheet-resources-app.gif)

On top of this project I added a REST API, and hosted it as well as an API proxy with caching, rate limiting, and other features in Apigee for API Management.

<img src="img/kaleo-healthcare-spec.png" width="500" />

From here I built a simple frontend to test the API, and see how optimizing the data being passed to a web client could be done with low/no-code in the API proxy.  

<img src="img/hospital-app.gif" width="300" />

Since calling the Google Sheet as a backend was quite slow (500-900ms), I added a caching policy to the backend, and got much better response times (50-100ms).

## Architecture

![Kaleo Healthcare Architecture Overview](img/kaleo-healthcare-architecture.png)

All in all this was a fun little project, and always cool to see how APIs and API proxies can help organize raw data, increase performance, and enable new apps utilizing existing backends.
