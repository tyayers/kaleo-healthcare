const routes = [
  { path: '/', name: "home", component: Vue.component("home") }
];

const router = new VueRouter({
  routes
});

axios.defaults.baseURL = "https://emea-poc13-test.apigee.net/kaleo-healthcare";

var app = new Vue({
  router: router,
  el: '#app',
  data: {
    menuVisible: false,
    perfTime: "",
    waiting: false   
  }
});

// Switch toggle
$('.Switch').click(function() {
  $(this).toggleClass('On').toggleClass('Off');

  if ($(this).hasClass('On')) {
    axios.defaults.baseURL = "https://emea-poc13-test.apigee.net/kaleo-healthcare";
  }
  else {
    axios.defaults.baseURL = "https://emea-poc13-test.apigee.net/kaleo-healthcare/v2";
  }
});