Vue.component('hospitals', {
  template: `
    <div>
      <article>
        <h1>Hospitals</h1>
        <span>{{lastCallTimeInMS}}</span>
        <center>
          <a style="margin: 1px; line-height: 1px; cursor: pointer;" v-for="hospitalid in hospitals" v-on:click="loadHospital(hospitalid);"><i v-if="hospital.hospitalId != hospitalid">{{hospitalid}}</i><b v-if="hospital.hospitalId == hospitalid">{{hospitalid}}</b></a>
        </center>
        <hr>
        <div class="container">
          <div class="panel post">
            <a><span>{{hospital.icuBedsOccupied}} / {{hospital.icuBedsCapacity}}</span>ICU Beds</a>
          </div>
          <div class="panel comment">
            <a><span>{{hospital.ventilatorsInUse}} / {{hospital.ventilatorsCapacity}}</span>Ventilators</a>
          </div>
          <div class="panel page">
            <a><span>{{hospital.glovesAvailable}}</span>Gloves</a>
          </div>
          <div class="panel user">
            <a><span>{{hospital.visorsAvailable}}</span>Visors</a>
          </div>
        </div>
      </article>
    </div>
    `,
    data: function() {
      return {
        hospitals: [],
        hospital: {},
        selectedId: "",
        lastCallTimeInMS: ""
      }
    },
    created () {
      this.selectedId = this.$route.params.id;
      this.init();
    }, 
    methods: {
      init() {
        var startTime = new Date();
        this.$root.waiting = true;
        axios("hospitals?apikey=umkGdewOtHBIlkpCYCAMXXYN7oM9qZPG").then(response => {
          this.hospitals = response.data.hospitals;

          if (!this.selectedId)
            this.selectedId = this.hospitals[0];
            axios("hospitals/" + this.selectedId + "?apikey=umkGdewOtHBIlkpCYCAMXXYN7oM9qZPG").then(response => {
              this.$root.waiting = false;
              this.$root.perfTime = ((new Date()) - startTime.getTime()) + " ms";
              this.hospital = response.data;
          });
        }).catch(error => {
          this.$root.waiting = false;
        });                     
      },
      loadHospital(hospitalId) {
        this.$root.waiting = true;
        var startTime = new Date();
        this.selectedId = hospitalId;
        axios("hospitals/" + this.selectedId + "?apikey=umkGdewOtHBIlkpCYCAMXXYN7oM9qZPG").then(response => {
          this.$root.perfTime = ((new Date()) - startTime.getTime()) + " ms";
          this.$root.waiting = false;
          this.hospital = response.data;
        });
      }
    }
});    
