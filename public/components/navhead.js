Vue.component('navhead', {
  template: `
    <header>
      <nav style="margin-bottom: 0px;">
        <div>
          <span style="position: relative; top: -4px;">Caching: </span>
          <div class="Switch On">
            <div class="Toggle"></div>
            <span class="On">ON</span>
            <span class="Off">OFF</span>
          </div>    
        </div>
        <ul>
          <li>{{this.$root.perfTime}}</li>
        </ul>    
      </nav>
      <img src="img/hospital-icon.png" width="200px"/>
      <h1>Hospital Resource <i>Tracker</i></h1>
    </header>
    `,
    data: function() {
      return {

      }
    },
    created () {
      this.init();
    }, 
    methods: {
      init() {
                               
      }
    }
});    
