Vue.component('home', {
  template: `
    <div>
      <navhead />
      <div v-show="this.$root.waiting" class="wait-container">
        <div class="ball"></div>
        <div class="ball"></div>
        <div class="ball"></div>
        <div class="ball"></div>
        <div class="ball"></div>
        <div class="ball"></div>
        <div class="ball"></div>
      </div>
      <main>
        <section>
          <hospitals/>
        </section>
      </main>
    </div>
    `,
    data: function() {
      return {

      }
    },
    created () {
      this.init();
    }, 
    methods: {
      init() {
                               
      }
    }
});    
