var res = JSON.parse(context.proxyResponse.content);
var hospitalId = context.getVariable("hospital.id");

if (hospitalId === null) {
    
    var newResponse = {
        "hospitals": []
    };    
    
    for(var i=0; i<res.feed.entry.length; i++) {
        
        var entry = res.feed.entry[i];
        if (entry['gs$cell']['col'] == 1 && entry['gs$cell']['row'] > 1) {
            print(JSON.stringify(entry));
            newResponse.hospitals.push(entry['gs$cell']['inputValue']);
        }
    }   
}
else {
    var newResponse = {
        "hospitalId": hospitalId
    };
    
    var hospitalRow;
    for(var i=0; i<res.feed.entry.length; i++) {
        
        var entry = res.feed.entry[i];
        if (entry['gs$cell']['$t'] == hospitalId) {
            hospitalRow = entry['gs$cell']['row'];
            break;
        }
    }
    
    for(var i=0; i<res.feed.entry.length; i++) {
        
        var entry = res.feed.entry[i];
        if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 4) {
            newResponse["icuBedsOccupied"] = entry['gs$cell']['$t'];
        }
        else if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 5) {
            newResponse["icuBedsCapacity"] = entry['gs$cell']['$t'];
        }    
        else if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 13) {
            newResponse["ventilatorsInUse"] = entry['gs$cell']['$t'];
        }  
        else if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 14) {
            newResponse["ventilatorsCapacity"] = entry['gs$cell']['$t'];
        }   
        else if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 15) {
            newResponse["glovesAvailable"] = entry['gs$cell']['$t'];
        }   
        else if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 16) {
            newResponse["apronsAvailable"] = entry['gs$cell']['$t'];
        }    
        else if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 17) {
            newResponse["gownsAvailable"] = entry['gs$cell']['$t'];
        }   
        else if (entry['gs$cell']['row'] == hospitalRow && entry['gs$cell']['col'] == 19) {
            newResponse["visorsAvailable"] = entry['gs$cell']['$t'];
        }
    }
}

context.proxyResponse.content = JSON.stringify(newResponse);
